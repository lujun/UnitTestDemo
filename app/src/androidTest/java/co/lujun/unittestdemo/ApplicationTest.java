package co.lujun.unittestdemo;

import android.test.ApplicationTestCase;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<MyApplication> {

    private MyApplication mApplication;

    public ApplicationTest() {
        super(MyApplication.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        // createApplication(), Start the Application under test,
        // in the same way as if it was started by the system.
        createApplication();
        mApplication = getApplication();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // 测试Object是否为null
    public void testObjectIsNull(){
        assertNotNull(mApplication.getObject());
    }
    // 赋值后测试Object是否为null
    public void testObjectIsNull2(){
        mApplication.setObject();
        assertNotNull(mApplication.getObject());
    }
}