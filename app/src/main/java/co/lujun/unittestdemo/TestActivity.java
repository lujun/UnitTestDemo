package co.lujun.unittestdemo;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;

/**
 * Created by lujun on 2015/10/8.
 */
public class TestActivity extends ActivityInstrumentationTestCase2<MainActivity> {

    private MainActivity mActivity;
    private Instrumentation mInstrumentation;

    public TestActivity(){
        super(MainActivity.class);
    }

    // initial touch mode for the Activity under test
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        // turn off touch mode, if do not do this, the key events are ignored
        setActivityInitialTouchMode(false);
        mActivity = getActivity();
        mInstrumentation = getInstrumentation();
    }

    // inject a customized Intent into the Activity under test
    @Override
    protected void tearDown() throws Exception {
        mActivity.finish();
        try {
            super.tearDown();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // 测试TextView是否为null
    public void testTextViewIsNull(){
        assertNotNull(mActivity.getTextView());
    }

    // 测试Button是否为null
    public void testButtonIsNull(){
        assertNotNull(mActivity.getButton());
    }

    // 测试TextView的内容是否与"activity_test"相等
    public void testTextViewText(){
        assertEquals("activity_test", mActivity.getTextView().getText());
    }

    // 测试Button的内容是否与"button"相等
    public void testButtonText(){
        assertEquals("button", mActivity.getButton().getText());
    }

    // Button接收焦点（key event）
    public void testButtonRequestFocus(){
        /*
         * request focus for the Spinner, so that the test can send key events to it
         * This request must be run on the UI thread. To do this, use the runOnUiThread method
         * and pass it a Runnable that contains a call to requestFocus on the Spinner.
         */
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mActivity.getButton().requestFocus();
            }
        });
    }
}
