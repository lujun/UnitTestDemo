package co.lujun.unittestdemo;

import android.content.Intent;
import android.test.ServiceTestCase;
import android.test.suitebuilder.annotation.MediumTest;

/**
 * Created by lujun on 2015/10/10.
 */
public class TestService extends ServiceTestCase<MyService> {

    private Intent mIntent;

    public TestService() {
        super(MyService.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getContext().startService(mIntent = new Intent(getContext(), MyService.class));
    }

    @Override
    protected void tearDown() throws Exception {
        getContext().stopService(mIntent);
    }

    // 测试是否相等
    @MediumTest
    public void testEqual() {
        assertEquals(1, 3);
    }
}