package co.lujun.unittestdemo;

import android.app.Application;

/**
 * Created by lujun on 2015/10/8.
 */
public class MyApplication extends Application {

    private Object mObject;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public Object getObject(){
        return mObject;
    }

    public void setObject(){
        mObject = new Object();
    }
}
