package co.lujun.unittestdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.btn_activity_test);
        textView = (TextView) findViewById(R.id.tv_activity_test);
    }

    public TextView getTextView(){
        return textView;
    }

    public Button getButton(){
        return button;
    }
}
